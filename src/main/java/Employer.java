public class Employer {

    private String employerId;

    private String employerName;

    private Person person = new Person();

    public void displayPersonDetails(){

        System.out.println(person.getId() + " : " +  person.getName());
    }

    public void displayemployerDetails(){

        System.out.println(getEmployerId() + " : " + getEmployerName());
    }

    public String getEmployerId() {
        return employerId;
    }

    public Employer setEmployerId(String employerId) {
        this.employerId = employerId;
        return this;
    }

    public String getEmployerName() {
        return employerName;
    }

    public Employer setEmployerName(String employerName) {
        this.employerName = employerName;
        return this;
    }

    public Person getPerson() {
        return person;
    }

    public Employer setPerson(Person person) {
        this.person = person;
        return this;
    }
}
