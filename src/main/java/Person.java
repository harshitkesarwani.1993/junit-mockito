public class Person {

    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public String displayDetails(String id, String name){

        return id + ":" + name;
    }

    public Person setId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Person setName(String name) {
        this.name = name;
        return this;
    }
}
