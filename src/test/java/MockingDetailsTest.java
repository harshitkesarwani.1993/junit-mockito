import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockingDetails;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mockingDetails;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MockingDetailsTest {

    @Mock
    LinkedList mockedList;

    @Test
    public void testMockingDetails(){

        when(mockedList.get(0)).thenReturn("first");

        doReturn("getFirst").when(mockedList).getFirst();

        mockedList.get(0);
        mockedList.getFirst();
        mockedList.get(999);

        //To identify whether a particular object is a mock or a spy:
        System.out.println(mockingDetails(mockedList).isMock());
        System.out.println(mockingDetails(mockedList).isSpy());

        //Getting details like type to mock or default answer:
        MockingDetails details = mockingDetails(mockedList);
        System.out.println(details.getMockCreationSettings().getTypeToMock());
        System.out.println(details.getMockCreationSettings().getDefaultAnswer());

        //Getting invocations and stubbings of the mock:
        System.out.println(details.getInvocations());
        System.out.println(details.getStubbings());

        //Printing all interactions (including stubbing, unused stubs)
        System.out.println(mockingDetails(mockedList).printInvocations());
    }
}
