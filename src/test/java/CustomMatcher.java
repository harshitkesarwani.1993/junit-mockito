import org.mockito.ArgumentMatcher;

public class CustomMatcher implements ArgumentMatcher<String> {

    @Override
    public boolean matches(String argument) {
        if(!(argument instanceof String))
            return false;
        String s = (String) argument;
        if(s.equalsIgnoreCase("element"))
            return  true;
        return  false;
    }

    public String toString() {
        //printed in verification errors
        return "Invalid input: Not 'element' ";
    }
}
