import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.ignoreStubs;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IgnoreStubsTest {

    @Mock
    List mock1, mock2;

    @Test
    public void testIgnoreStub(){

       //stubbing mocks:
       when(mock1.get(0)).thenReturn(10);
       when(mock2.get(0)).thenReturn(20);

       //using mocks by calling stubbed get(0) methods:
       System.out.println(mock1.get(0)); //prints 10
       System.out.println(mock2.get(0)); //prints 20

       //using mocks by calling clear() methods:
       mock1.clear();
       mock2.clear();

       //verification:
       verify(mock1).clear();
       verify(mock2).clear();

       //verifyNoMoreInteractions() fails because get() methods were not accounted for.
       //verifyNoMoreInteractions(mock1, ignoreStubs(mock2)); //this will not work
//       verifyNoMoreInteractions(mock1, mock2);

       //However, if we ignore stubbed methods then we can verifyNoMoreInteractions()
       verifyNoMoreInteractions(ignoreStubs(mock1, mock2));
    }

}
