import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StubbingExamples {

    @Mock
    List mock;

    @Before
    public void setUp(){

        when(mock.get(0)).thenReturn("firstSetUpCall").thenReturn("secondSetUpCall");
        when(mock.get(3)).thenReturn("firstSetUpCall").thenReturn("secondSetUpCall");
    }

    @Test
    public void testConsecutiveCalls() {
        when(mock.get(1)).thenReturn("firstCall").thenReturn("secondCall");

        //First call: prints "firstCall"
        System.out.println(mock.get(1));

        //Second call: prints "secondCall"
        System.out.println(mock.get(1));

        //Any consecutive call: prints "secondCall" as well (last stubbing wins).
        System.out.println(mock.get(1));

        when(mock.get(2)).thenReturn("one", "two", "three");
        System.out.println(mock.get(2));
        System.out.println(mock.get(2));
        System.out.println(mock.get(2));

//      All mock.someMethod("some arg") calls will return "two"
//      when(mock.someMethod("some arg")).thenReturn("one")
//      when(mock.someMethod("some arg")).thenReturn("two")

        //Calling setup mock
        System.out.println(mock.get(0));
        System.out.println(mock.get(3));
        System.out.println(mock.get(3));
    }

    //consecutive calls doesn't work for different test methods
    @Test
    public void testConsecutiveBefore(){

        System.out.println(mock.get(0) + " - method2");
    }
}
