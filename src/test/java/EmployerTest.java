import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmployerTest {

    @Mock
    Person personMock;

    @Mock
    Employer employerMock;

    @InjectMocks
    Employer employer = new Employer();

    @Test
    public void displayPersonDetails() {

        when(personMock.getId()).thenReturn("1");
        given(personMock.getName()).willReturn("Person-1");
        employer.displayPersonDetails();
    }

    @Test
    public void exampleThenAnswer() {

        when(personMock.getId()).thenReturn("1");
        when(personMock.displayDetails(anyString(), anyString())).thenAnswer((Answer<String>) invocation -> {return (String) invocation.getArguments()[0] + " : called with custom answer";});
        System.out.println(personMock.displayDetails("2", "Person-2"));
        employer.displayPersonDetails();
    }

    @Test
    public void exampleDoCallRealMethod() {

        when(personMock.getId()).thenReturn("1");
        doCallRealMethod().when(personMock).displayDetails(anyString(), anyString());
        System.out.println(personMock.displayDetails("2", "Person-2"));
        employer.displayPersonDetails();
    }
}