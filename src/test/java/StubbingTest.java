import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class demonstrates the two phases
 * 1. Stubbing
 * 2. Verification
 *
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class StubbingTest {

    @Mock
    LinkedList mockedList;

    @Test
    public void testStubbing() {

        //stubbing
        when(mockedList.get(0)).thenReturn("first");

        //will throw unnecessary exception as it is overridden, this will throw in latest mockito versions
//        when(mockedList.getFirst()).thenThrow(new RuntimeException());
        doReturn("getFirst").when(mockedList).getFirst();

//        doThrow(new RuntimeException()).when(mockedList).clear();
        //following throws RuntimeException:

        //following prints "first"
        System.out.println(mockedList.get(0));

        //following throws runtime exception
        System.out.println(mockedList.getFirst());

        //following prints "null" because get(999) was not stubbed
        System.out.println(mockedList.get(999));

        //Although it is possible to verify a stubbed invocation, usually it's just redundant
        //If your code cares what get(0) returns, then something else breaks (often even before verify() gets executed).
        //If your code doesn't care what get(0) returns, then it should not be stubbed.
        verify(mockedList).get(0);

//        mockedList.clear();
    }

}
