import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.description;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class VerifyExamples {

    // A. Single mock whose methods must be invoked in a particular order
    @Mock
    List singleMock;

    // B. Multiple mocks that must be used in a particular order
    @Mock
    List firstMock;
    @Mock
    List secondMock;

    @Mock
    List mockOne;
    @Mock
    List mockTwo;

    @Captor
    ArgumentCaptor<List> argumentCaptor;

    @Test
    public void testVerifyInOrder(){

        //create an inOrder verifier for a single mock
        InOrder singleInOrder = inOrder(singleMock);

        //create inOrder object passing any mocks that need to be verified in order
        InOrder twoInOrder = inOrder(firstMock, secondMock);

        //using a single mock
        singleMock.add("was added first");
        singleMock.get(0);

        //If added then it should be verified otherwise verifyNoMoreInteractions will fail
        //singleMock.get(1);

        //following will make sure that add is first called with "was added first", then get()
        singleInOrder.verify(singleMock).add("was added first");

        //It checks that get(0) should be called once and it can be more than one
        //singleInOrder.verify(singleMock, calls( 1)).get(0);

        singleInOrder.verify(singleMock).get(0);

        //using mocks
        firstMock.add("was called first");
        secondMock.add("was called second");

        //following will make sure that firstMock was called before secondMock
        twoInOrder.verify(firstMock).add("was called first");
        twoInOrder.verify(secondMock).add("was called second");

        //verify that other mocks were not interacted
        //mockOne.clear();
        verifyZeroInteractions(mockOne, mockTwo);

        verifyNoMoreInteractions(singleMock);
    }

    @Test
    public void testNumberOfInvocations() {
        //using mock
        mockOne.add("once");
//        mockOne.add("once");

        mockOne.add("twice");
        mockOne.add("twice");

        mockOne.add("three times");
        mockOne.add("three times");
        mockOne.add("three times");

        //following two verifications work exactly the same - times(1) is used by default
        verify(mockOne, description("This will print on failure")).add("once");
        verify(mockOne, times(1)).add("once");

        //exact number of invocations verification
        verify(mockOne, times(6)).add(argumentCaptor.capture());
        System.out.println(argumentCaptor.getAllValues());
        verify(mockOne, times(3)).add("three times");

        //verification using never(). never() is an alias to times(0)
        verify(mockOne, never()).add("never");

        //verification using atLeast()/atMost()
        verify(mockOne, atMost(1)).add("once");
        verify(mockOne, atLeastOnce()).add("twice");
        verify(mockOne, atLeast(2)).add("three times");
        verify(mockOne, atMost(5)).add("three times");
    }
}
