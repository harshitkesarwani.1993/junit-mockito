import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StrictStubsTest {

    @Mock
    List mock;

    @Test
    public void testIgnoreStub() {

        //stubbing mocks:
        when(mock.get(eq(1))).thenReturn(10);
        when(mock.get(eq(0))).thenReturn(20);

        //using mocks by calling stubbed get(0) methods:
        System.out.println(mock.get(1));
        System.out.println(mock.get(0));

    }
}