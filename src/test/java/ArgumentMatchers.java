import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ArgumentMatchers {

    @Mock
    List mockedList;

    private List<String> stringList = new ArrayList<>();

    @Test
    public void testArgumentMatchers() {
        //stubbing using built-in anyInt() argument matcher
        //*****tell difference between any, eq, and actual argument*******
        when(mockedList.get(anyInt())).thenReturn("element");

        //stubbing using custom matcher (let's say isValid() returns your own matcher implementation):
        when(mockedList.contains(argThat(new CustomMatcher()))).thenReturn(true);

        when(mockedList.addAll(anyInt(), eq(stringList))).thenReturn(true);

        //following prints "element"
        System.out.println(mockedList.get(999));

        assertFalse(!mockedList.contains("element"));

        System.out.println(mockedList.addAll(1, stringList));

        //you can also verify using an argument matcher
        verify(mockedList).get(anyInt());

        verify(mockedList).contains(argThat(new CustomMatcher()));

        verify(mockedList).addAll(anyInt(), eq(stringList));
        //above is correct - eq() is also an argument matcher
//        verify(mockedList).addAll(anyInt(), stringList);
        //above is incorrect - exception will be thrown because third argument is given without an argument matcher.
    }

}
